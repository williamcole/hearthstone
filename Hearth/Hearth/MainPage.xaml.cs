﻿using System;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace Hearth
{
    public partial class MainPage : MasterDetailPage
    {
        public MainPage()
        {
            InitializeComponent();

            App.GetTheme();
            masterPage.MasterListView.ItemSelected += OnItemSelected;
        }

        void OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var item = e.SelectedItem as MasterPageItem;

            if (item != null)
            {
                Detail = new NavigationPage((Page)Activator.CreateInstance(item.TargetType));

                masterPage.MasterListView.SelectedItem = null;
                IsPresented = false;
            }
        }

        private void Handle_IsPresentedChanged(object sender, EventArgs e)
        {
            App.GetTheme();
        }
    }
}
