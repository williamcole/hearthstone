﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hearth
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SingleCardPage : ContentPage
	{
        private CardModel _card;

		public SingleCardPage (CardModel card)
		{
			InitializeComponent ();

            App.GetTheme();
            _card = card;
            BindingContext = _card;

            PopulateCarouselView();
		}

        private void PopulateCarouselView()
        {
            CardCarouselView.ItemsSource = new List<Image>()
            {
                new Image()
                {
                    Source = new Uri(_card.Img)
                },

                new Image()
                {
                    Source = new Uri(_card.ImgGold)
                }
            };
        }
    }
}