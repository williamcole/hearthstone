﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Newtonsoft.Json;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace Hearth
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new MainPage();
            GetTheme();
        }

        static HearthApi hearthApi;

        public static HearthApi HSApi
        {
            get
            {
                if (hearthApi == null)
                {
                    hearthApi = new HearthApi();
                }

                return hearthApi;
            }
        }

        public static ThemeModel Theme
        {
            get
            {
                return GetTheme();
            }
        }

        public static ThemeModel GetTheme()
        {
            ThemeModel theme = JsonConvert.DeserializeObject<ThemeModel>(Settings.GeneralSettings);

            if (theme != null)
            {
                Current.Resources["BackgroundColor"] = Color.FromHex(theme.BackgroundColorTheme);
                Current.Resources["PrimaryTextColor"] = Color.FromHex(theme.PrimaryTextColorTheme);
                Current.Resources["SecondaryTextColor"] = Color.FromHex(theme.SecondaryTextColorTheme);
                return theme;
            }

            return null;
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
