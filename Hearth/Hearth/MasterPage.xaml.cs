﻿using Newtonsoft.Json;
using Xamarin.Forms;

namespace Hearth
{
    public partial class MasterPage : ContentPage
    {
        public MasterPage()
        {
            InitializeComponent();
            App.GetTheme();
        }
    }
}
