﻿using Newtonsoft.Json;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hearth
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ThemesPage : ContentPage
    {
        public ThemesPage()
        {
            InitializeComponent();

            App.GetTheme();

            if (App.Theme != null)
            {
                SwitchCellSlide.On = App.Theme.ThemeEnabled;
            }
        } 

        private void SwitchCell_OnDarkModeEnabled(object sender, ToggledEventArgs e)
        {
            if(SwitchCellSlide.On == false)
            {
                ResetDefaultMode();
                return;
            }

            ThemeModel theme = new ThemeModel
            {
                BackgroundColorTheme = "#3B4345",
                PrimaryTextColorTheme = "C8CED0",
                SecondaryTextColorTheme = "CAC9C9",
                ThemeEnabled = true
            };

            string stringTheme = JsonConvert.SerializeObject(theme);
            Settings.GeneralSettings = stringTheme;

            Resources["BackgroundColor"] = Color.FromHex(theme.BackgroundColorTheme);
            Resources["PrimaryTextColor"] = Color.FromHex(theme.PrimaryTextColorTheme); 
            Resources["SecondaryTextColor"] = Color.FromHex(theme.SecondaryTextColorTheme);
        }

        public void ResetDefaultMode()
        {
            ThemeModel theme = new ThemeModel
            {
                BackgroundColorTheme = "FFFFFF",
                PrimaryTextColorTheme = "212121",
                SecondaryTextColorTheme = "5B5A5A",
                ThemeEnabled = false
            };

            string stringTheme = JsonConvert.SerializeObject(theme);
            Settings.GeneralSettings = stringTheme;

            Resources["BackgroundColor"] = Color.FromHex("FFFFFF");
            Resources["PrimaryTextColor"] = Color.FromHex("212121");
            Resources["SecondaryTextColor"] = Color.FromHex("5B5A5A");
        } 
    }
}