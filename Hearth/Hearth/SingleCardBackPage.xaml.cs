﻿using Newtonsoft.Json;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hearth
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SingleCardBackPage : ContentPage
	{
		public SingleCardBackPage (CardBackModel cardBack)
		{
			InitializeComponent ();

            App.GetTheme();
            BindingContext = cardBack;
		}
	}
}