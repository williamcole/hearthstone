﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hearth
{
    public class ThemeModel
    {
        public string BackgroundColorTheme { get; set; }
        public string PrimaryTextColorTheme { get; set; }
        public string SecondaryTextColorTheme { get; set; }
        public bool ThemeEnabled { get; set; }
    }    
}
