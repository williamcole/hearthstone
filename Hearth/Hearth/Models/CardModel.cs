﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using Newtonsoft.Json;

namespace Hearth
{
    public class CardModel
    {
        public string CardId { get; set; }
        public string Name { get; set; }
        public string CardSet { get; set; } = "None";
        public string Type { get; set; }
        public string Faction { get; set; }
        public string Rarity { get; set; }
        public int Cost { get; set; }
        public int Attack { get; set; } = 0;
        public int Health { get; set; } = 0;
        public string Text { get; set; } = "None";
        public string Flavor { get; set; } = "None";
        public string Artist { get; set; } = "None";
        public bool Collectible { get; set; }
        public bool Elite { get; set; }
        public string Race { get; set; } = "None";
        public string PlayerClass { get; set; } = "None";
        public string MultiClassGroup { get; set; } = "None";
        public List<string> Classes;
        public string Img { get; set; }
        public string ImgGold { get; set; }
        public string Locale { get; set; }

        [JsonIgnore]
        public Color ClassColor { get; set; }

        [JsonIgnore]
        public Color RarityColor { get; set; }

        [JsonIgnore]
        public List<string> Mechanics { get; set; }

        public CardModel()
        {
        }

        public void SetRarityColors()
        {
            switch(Rarity)
            {
                case "Common":
                    RarityColor = Color.Gray;
                    break;
                case "Uncommon":
                    RarityColor = Color.FromHex("#1eff00");
                    break;
                case "Rare":
                    RarityColor = Color.FromHex("#0070dd");
                    break;
                case "Epic":
                    RarityColor = Color.FromHex("#a335ee");
                    break;
                case "Legendary":
                    RarityColor = Color.FromHex("#ff8000");
                    break;
                case "Free":
                    RarityColor = Color.FromHex("#9d9d9d");
                    break;
                default:
                    RarityColor = Color.Black;
                    break;
            }
        }

        public void SetClassColors()
        {
            switch (PlayerClass)
            {
                case "Druid":
                    ClassColor = Color.FromHex("#FF7D0A");
                    break;
                case "Hunter":
                    ClassColor = Color.FromHex("#ABD473");
                    break;
                case "Mage":
                    ClassColor = Color.FromHex("#40C7EB");
                    break;
                case "Paladin":
                    ClassColor = Color.FromHex("#F58CBA");
                    break;
                case "Priest":
                    ClassColor = Color.LightGray;
                    break;
                case "Rogue":
                    ClassColor = Color.Yellow;
                    break;
                case "Shaman":
                    ClassColor = Color.FromHex("#0070DE");
                    break;
                case "Warlock":
                    ClassColor = Color.FromHex("#8787ED");
                    break;
                case "Warrior":
                    ClassColor = Color.FromHex("#C79C6E");
                    break;
                case "Neutral":
                    ClassColor = Color.Goldenrod;
                    break;
                default:
                    ClassColor = Color.Black;
                    break;
            }
        }

    }
}
