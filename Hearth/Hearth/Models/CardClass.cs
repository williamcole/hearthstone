﻿using Xamarin.Forms;

namespace Hearth
{
    public class CardClass
    {
        public string Picture { get; set; }
        public string Name { get; set; }
        public Color ClassColor { get; set; }
    }
}
