﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hearth
{
    public class CardBackModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Img { get; set; }
        public string HowToGet { get; set; }
    }
}
