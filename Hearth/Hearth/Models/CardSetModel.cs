﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hearth
{
    public class CardSetModel
    {
        public string Picture { get; set; }
        public string Name { get; set; }
    }
}
