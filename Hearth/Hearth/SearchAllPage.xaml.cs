﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Newtonsoft.Json;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hearth
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SearchAllPage : ContentPage
    {
        public SearchAllPage()
        {
            InitializeComponent();

            App.GetTheme();
            ResultListView.IsVisible = false;
            ResultLabel.IsVisible    = false;
        }

        private async void Handle_SearchCard(object sender, TextChangedEventArgs e)
        {
            if (!String.IsNullOrEmpty(SearchBox.Text))
            {
                var cardResults = await App.HSApi.SearchByPartialName(SearchBox.Text);

                if (cardResults == null || cardResults.Count < 1)
                {
                    ResultLabel.IsVisible = true;
                    ResultListView.IsVisible = false;
                }
                else
                {
                    PopulateSearchList(cardResults);
                }
            }
            else
            {
                ResultListView.IsVisible = false;
            }
        }

        private void Handle_ShowSingleCard(object sender, SelectedItemChangedEventArgs e)
        {
            CardModel cardToShow = e.SelectedItem as CardModel;

            if (cardToShow != null)
            {
                Navigation.PushAsync(new SingleCardPage(cardToShow));
            }
        }

        private void PopulateSearchList(List<CardModel> results)
        {
            var resultsCollection = new ObservableCollection<CardModel>(results);

            ResultLabel.IsVisible = false;
            ResultListView.IsVisible = true;

            ResultListView.ItemsSource = resultsCollection;
        }
    }
}