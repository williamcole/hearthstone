﻿using Newtonsoft.Json;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hearth
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CardBacksPages : ContentPage
	{
		public CardBacksPages ()
		{
			InitializeComponent ();
            App.GetTheme();
            PopulateCardBackList();
		}

        public async void PopulateCardBackList()
        {
            var cardBacks = await App.HSApi.GetCardBacks();

            CardBackListView.ItemsSource = cardBacks;
        }

        private async void Handle_CardBackTapped(object sender, ItemTappedEventArgs e)
        {
            var cardBackListView = (ListView)sender;
            var cardBack = (CardBackModel)cardBackListView.SelectedItem;

            await Navigation.PushAsync(new SingleCardBackPage(cardBack));
        }
    }
}