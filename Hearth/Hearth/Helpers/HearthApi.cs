﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Collections.Generic;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace Hearth
{
    public class HearthApi
    {
        private HttpClient _client;

        public HearthApi()
        {
            _client = new HttpClient();
            _client.DefaultRequestHeaders.Add("X-Mashape-Key", ApiKey.Key);
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public async Task<List<CardModel>> GetAllCards()
        {
            string url = "https://omgvamp-hearthstone-v1.p.mashape.com/cards";

            string jsonContent = await GetJsonFromEndpoint(url);
            var cardList = JsonToModel<CardModel>(jsonContent);

            if (cardList.Count > 0)
            {
                return cardList;
            }

            return null;
        }

        public async Task<List<CardModel>> SearchByPartialName(string name)
        {
            if (String.IsNullOrEmpty(name))
            {
                return null;
            }

            name = name.Replace(" ", "%20");

            string url = "https://omgvamp-hearthstone-v1.p.mashape.com/cards/search/" + name;

            string jsonContent =  await GetJsonFromEndpoint(url);

            if (jsonContent == null)
            {
                return null;
            }

            var searchResults = JsonToModel<CardModel>(jsonContent);

            FormatCardsText(searchResults);
            FilterResults(searchResults);
            SetCardColors(searchResults);

            return searchResults;

        }

        public async Task<List<CardBackModel>> GetCardBacks()
        {
            string url = "https://omgvamp-hearthstone-v1.p.mashape.com/cardbacks";

            var jsonContent = await GetJsonFromEndpoint(url);

            if (jsonContent == null)
            {
                return null;
            }

            var cardBacks = JsonToModel<CardBackModel>(jsonContent);

            return cardBacks;
        }

        public async Task<CardModel> GetCardByName(string cardName)
        {
            cardName = cardName.Replace(" ", "%20");

            string url = "https://omgvamp-hearthstone-v1.p.mashape.com/cards/" + cardName;

            var jsonContent = await GetJsonFromEndpoint(url);
            var card = JsonToModel<CardModel>(jsonContent).First();

            return card;
        }
        
        public async Task<List<CardModel>> GetCardsByClass(string className)
        {
            string url = "https://omgvamp-hearthstone-v1.p.mashape.com/cards/classes/" + className;

            var jsonContent = await GetJsonFromEndpoint(url);
            var classCards = JsonToModel<CardModel>(jsonContent);

            // Remove cards that aren't Minions, Spells, or Weapons
            // Do not return Enchantments, Hero Powers, or Heros
            FilterResults(classCards);
            FormatCardsText(classCards);
            SetCardColors(classCards);

            return classCards;
        }

        public async Task<List<CardModel>> GetCardsBySet(string setName )
        {
            setName = setName.Replace(" ", "%20");

            string url = "https://omgvamp-hearthstone-v1.p.mashape.com/cards/sets/" + setName;

            var jsonContent = await GetJsonFromEndpoint(url);

            if (jsonContent == null)
            {
                return null;
            }

            var setCards = JsonToModel<CardModel>(jsonContent);
            FilterResults(setCards);
            FormatCardsText(setCards);
            SetCardColors(setCards);

            return setCards;
        }

        public async Task<List<CardModel>> GetCardsByRarity(string rarity)
        {
            string url = "https://omgvamp-hearthstone-v1.p.mashape.com/cards/qualities/" + rarity;

            var jsonContent = await GetJsonFromEndpoint(url);

            if (jsonContent == null)
            {
                return null;
            }

            var rarityCards = JsonToModel<CardModel>(jsonContent);

            return rarityCards;
        }
        
        public async Task<GameInfo> GetGameInfo()
        {
            string url = "https://omgvamp-hearthstone-v1.p.mashape.com/info";

            var jsonContent = await GetJsonFromEndpoint(url);

            if (jsonContent == null)
            {
                return null;
            }

            // Getting info from the API doesn't return the json as a list, so convert it to one
            jsonContent = "[" + jsonContent + "]";

            var gameInfo = JsonToModel<GameInfo>(jsonContent).First();
            return gameInfo;
        }

        // Web API had text with random tags embedded in it, so remove them
        public void FormatCardsText(List<CardModel> cards)
        {
            foreach( var card in cards)
            {
                card.Text =
                    card.Text.Replace("_", " ")
                    .Replace("\\n", "")
                    .Replace("\n", " ")
                    .Replace("$", "")
                    .Replace("<b>", "")
                    .Replace("</b>", " ")
                    .Replace("<i>", "")
                    .Replace("</i>", " ");

                card.Flavor = 
                    card.Flavor.Replace("_", " ")
                    .Replace("\\n", "")
                    .Replace("\n", " ")
                    .Replace("$", "")
                    .Replace("<b>", "")
                    .Replace("</b>", " ")
                    .Replace("<i>", "")
                    .Replace("</i>", " ");
            }
        }

        public void SetCardColors(List<CardModel> cards)
        {
            foreach (var card in cards)
            {
                card.SetRarityColors();
                card.SetClassColors();
            }
        }

        public void FilterResults(List<CardModel> cards)
        {
            cards.RemoveAll(card =>
                                         card.Type.Equals("Hero")
                                      ||  card.Type.Equals("Enchantment")
                                      || card.Type.Equals("Hero Power")
                                      || card.CardSet.Equals("Credits")
                                      || card.CardSet.Equals("Tavern Brawl")
                                      || String.IsNullOrEmpty(card.Rarity)
                                      || card.Collectible == false);
        }
        
        private List<T> JsonToModel<T>(string jsonContent)
        {
            var modelList = JsonConvert.DeserializeObject<List<T>>(jsonContent);
            return modelList;
        }

        private async Task<string> GetJsonFromEndpoint(string url)
        {
            var uri = new Uri(url);
            var response = await _client.GetAsync(uri);

            if (response.IsSuccessStatusCode)
            {
                string jsonContent = await response.Content.ReadAsStringAsync();

                return jsonContent;
            }

            return null;
        }
    }
}
