﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Collections.ObjectModel;

namespace Hearth
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class DeckBuilderPage : ContentPage
	{
        public ObservableCollection<CardModel> DeckCardList = new ObservableCollection<CardModel>();
 
    
		public DeckBuilderPage(CardClass className)
		{
			InitializeComponent ();
            App.GetTheme();

            CardModel Card = new CardModel();
            BindingContext = Card;

            PopulateCardsList(className.Name);
        }

        private async void PopulateCardsList(string className)
        {
            var CardList = await App.HSApi.GetCardsByClass(className);

           ListOfCards.ItemsSource = CardList;
        }

        private void ListOfCards_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            var Card = (ListView)sender;
            var GetCard = (CardModel)Card.SelectedItem;
            DeckCardList.Add(GetCard);
            DeckList.ItemsSource = DeckCardList;               
        }
      
    }
}