﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;


namespace Hearth
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CardsPage : ContentPage
    {     
        private string ClassName { get; set; }
        private string SetName   { get; set; }

        public CardsPage(CardClass cardClass)
        {
            InitializeComponent();
            App.GetTheme();
            BindingContext = cardClass;
            ClassName = cardClass.Name;
            PopulateCardsListByClass(ClassName);
        }

        public CardsPage(CardSetModel cardSet)
        {
            InitializeComponent();

            App.GetTheme();
            BindingContext = cardSet;
            SetName = cardSet.Name;
            PopulateCardsListBySet(SetName);
        }

        private async void PopulateCardsListByClass(string className)
        {
           var classCardsList =  await App.HSApi.GetCardsByClass(className);

            CardsListView.ItemsSource = classCardsList;
        }

        private async void PopulateCardsListBySet(string setName)
        {
            var setCardsList = await App.HSApi.GetCardsBySet(setName);

            CardsListView.ItemsSource = setCardsList;
        }

        private void Handle_ShowSingleCard(object sender, SelectedItemChangedEventArgs e)
        {
            CardModel cardToShow = e.SelectedItem as CardModel;

            if (cardToShow != null)
            {
                Navigation.PushAsync(new SingleCardPage(cardToShow));
            }
        }

        private async void PickerCardFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            Picker CardFilter = (Picker)sender;
            var FilterSelection = (string)CardFilter.SelectedItem;

            if (FilterSelection == "All")
            {
                var CardClassList = await App.HSApi.GetCardsByClass(ClassName);
                foreach (var card in CardClassList)
                {
                    card.SetClassColors();
                    card.SetRarityColors();
                }
                CardsListView.ItemsSource = CardClassList;
            }

            if (FilterSelection == "Rarity: Low to High")
            {
                var CardsByRarityLowToHigh = new List<CardModel>();

                CardsByRarityLowToHigh = await App.HSApi.GetCardsByRarity("Free");
                CardsByRarityLowToHigh.AddRange(  await App.HSApi.GetCardsByRarity("Common"));
                CardsByRarityLowToHigh.AddRange(await App.HSApi.GetCardsByRarity("Rare"));
                CardsByRarityLowToHigh.AddRange(await App.HSApi.GetCardsByRarity("Epic"));
                CardsByRarityLowToHigh.AddRange(await App.HSApi.GetCardsByRarity("Legendary"));

                CardsByRarityLowToHigh.RemoveAll(card => card.PlayerClass != ClassName);

                foreach (var card in CardsByRarityLowToHigh)
                {
                  
                    card.SetClassColors();
                    card.SetRarityColors();
                }

                CardsListView.ItemsSource = CardsByRarityLowToHigh;
                CardsListView.ScrollTo(CardsByRarityLowToHigh[0], ScrollToPosition.Start, true);
            }

            if (FilterSelection == "Rarity: High to Low")
            {
                var CardsByRarityHighToLow = new List<CardModel>();

                CardsByRarityHighToLow = await App.HSApi.GetCardsByRarity("Legendary");
                CardsByRarityHighToLow.AddRange(await App.HSApi.GetCardsByRarity("Epic"));
                CardsByRarityHighToLow.AddRange(await App.HSApi.GetCardsByRarity("Rare"));
                CardsByRarityHighToLow.AddRange(await App.HSApi.GetCardsByRarity("Common"));
                CardsByRarityHighToLow.AddRange(await App.HSApi.GetCardsByRarity("Free"));

                CardsByRarityHighToLow.RemoveAll(card => card.PlayerClass != ClassName);

                foreach (var card in CardsByRarityHighToLow)
                {
                    card.SetClassColors();
                    card.SetRarityColors();
                }

                CardsListView.ItemsSource = CardsByRarityHighToLow;
            }

            if(FilterSelection == "Rarity: Free")
            {
                var CardsByRarity = new List<CardModel>();

                CardsByRarity = await App.HSApi.GetCardsByRarity("Free");

                CardsByRarity.RemoveAll(card => card.PlayerClass != ClassName);

                foreach (var card in CardsByRarity)
                {
                    card.SetClassColors();
                    card.SetRarityColors();
                }

                CardsListView.ItemsSource = CardsByRarity;
            }

            if (FilterSelection == "Rarity: Common")
            {
                var CardsByRarity = new List<CardModel>();

                CardsByRarity = await App.HSApi.GetCardsByRarity("Common");

                CardsByRarity.RemoveAll(card => card.PlayerClass != ClassName);

                foreach (var card in CardsByRarity)
                {
                    card.SetClassColors();
                    card.SetRarityColors();
                }

                CardsListView.ItemsSource = CardsByRarity;
            }

            if (FilterSelection == "Rarity: Rare")
            {
                var CardsByRarity = new List<CardModel>();

                CardsByRarity = await App.HSApi.GetCardsByRarity("Rare");

                CardsByRarity.RemoveAll(card => card.PlayerClass != ClassName);

                foreach (var card in CardsByRarity)
                {
                    card.SetClassColors();
                    card.SetRarityColors();
                }

                CardsListView.ItemsSource = CardsByRarity;
            }

            if (FilterSelection == "Rarity: Epic")
            {
                var CardsByRarity = new List<CardModel>();

                CardsByRarity = await App.HSApi.GetCardsByRarity("Epic");

                CardsByRarity.RemoveAll(card => card.PlayerClass != ClassName);

                foreach (var card in CardsByRarity)
                {
                    card.SetClassColors();
                    card.SetRarityColors();
                }

                CardsListView.ItemsSource = CardsByRarity;
            }

            if (FilterSelection == "Rarity: Legendary")
            {
                var CardsByRarity = new List<CardModel>();

                CardsByRarity = await App.HSApi.GetCardsByRarity("Legendary");

                CardsByRarity.RemoveAll(card => card.PlayerClass != ClassName);

                foreach (var card in CardsByRarity)
                {
                    card.SetClassColors();
                    card.SetRarityColors();
                }

                CardsListView.ItemsSource = CardsByRarity;
                CardsListView.ScrollTo(CardsByRarity[0], ScrollToPosition.Start, true);
            }
        }
    }
}