﻿using System.Collections.Generic;
using Newtonsoft.Json;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hearth
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CardSetsPage : ContentPage
	{
		public CardSetsPage ()
		{
            InitializeComponent();

            ThemeModel theme = JsonConvert.DeserializeObject<ThemeModel>(Settings.GeneralSettings);

            if (theme != null)
            {
                Resources["BackgroundColor"] = Color.FromHex(theme.BackgroundColorTheme);
                Resources["PrimaryTextColor"] = Color.FromHex(theme.PrimaryTextColorTheme);
                Resources["SecondaryTextColor"] = Color.FromHex(theme.SecondaryTextColorTheme);
            }

            PopulateSetListView();
		}

        public void PopulateSetListView()
        {
           var setList = new List<CardSetModel>()
           {
               new CardSetModel()
               {
                   Picture = "classic.png",
                   Name = "Classic"
               },

               new CardSetModel()
               {
                   Picture = "naxx.png",
                   Name = "Naxxramas"
               },

               new CardSetModel()
               {
                   Picture = "gvg.png",
                   Name = "Goblins vs Gnomes"
               }, 

               new CardSetModel()
               {
                   Picture = "BlackrockMountain.png",
                   Name = "Blackrock Mountain"
               },

               new CardSetModel()
               {
                   Picture = "GrandTournamentLogo.png",
                   Name = "The Grand Tournament"
               },

               new CardSetModel()
               {
                   Picture = "explorers.png",
                   Name = "The League of Explorers"
               },

               new CardSetModel()
               {
                   Picture = "oldgods.png",
                   Name = "Whispers of the Old Gods"
               },

               new CardSetModel()
               {
                   Picture = "karazhan.png",
                   Name = "One Night in Karazhan"
               },

               new CardSetModel()
               {
                   Picture = "gadgetzan.png",
                   Name = "Mean Streets of Gadgetzan"
               },

               new CardSetModel()
               {
                   Picture = "ungoro.png",
                   Name = "Journey to Un'Goro"
               }, 

               new CardSetModel()
               {
                   Picture = "FrozenThrone.png",
                   Name = "Knights of the Frozen Throne"
               }, 

               new CardSetModel()
               {
                   Picture = "kobolds.png",
                   Name = "Kobolds and Catacombs"
               }, 

               new CardSetModel()
               {
                   Picture = "witchwood.png",
                   Name = "The Witchwood"
               }, 

               new CardSetModel()
               {
                   Picture = "boomsday.png",
                   Name = "The Boomsday Project"
               },

               new CardSetModel()
               {
                   Picture = "rumble.png",
                   Name = "Rastakhan's Rumble"
               }
           };

            CardSetsListView.ItemsSource = setList;
        }

        private async void Handle_CardSetTapped(object sender, ItemTappedEventArgs e)
        {
            var cardSetListView = (ListView)sender;
            var cardSet = (CardSetModel)cardSetListView.SelectedItem;

            await Navigation.PushAsync(new CardsPage(cardSet));
        }
    }
}