﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hearth
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class DeckBuilderClassSelectPage : ContentPage
	{
		public DeckBuilderClassSelectPage ()
		{
			InitializeComponent ();
            App.GetTheme();
            PopulateListView();
		}

        private void PopulateListView()
        {
            var CardClassList = new ObservableCollection<CardClass>()
            {
                new CardClass()
                {
                    Picture = "druidicon.png",
                    Name = "Druid",
                    ClassColor = Color.FromHex("#FF7D0A")
                },

                new CardClass()
                {
                    Picture = "huntericon.png",
                    Name = "Hunter",
                    ClassColor = Color.FromHex("#ABD473"),
                },

                new CardClass()
                {
                    Picture = "mageicon.png",
                    Name = "Mage",
                    ClassColor = Color.FromHex("#40C7EB")
                },

                new CardClass()
                {
                    Picture = "paladinicon.png",
                    Name = "Paladin",
                    ClassColor = Color.FromHex("#F58CBA")
                },

                new CardClass()
                {
                    Picture = "priesticon.png",
                    Name = "Priest",
                    ClassColor = Color.Gray
                },

                new CardClass()
                {
                    Picture = "rogueicon.png",
                    Name = "Rogue",
                    ClassColor = Color.FromHex("#FFF569")
                },

                new CardClass()
                {
                    Picture = "shamanicon.png",
                    Name = "Shaman",
                    ClassColor = Color.FromHex("#0070DE")
                },

                new CardClass()
                {
                    Picture = "warlockicon.png",
                    Name = "Warlock",
                    ClassColor = Color.FromHex("#8787ED")
                },

                new CardClass()
                {
                    Picture = "warrioricon.png",
                    Name = "Warrior",
                    ClassColor = Color.FromHex("#C79C6E")
                },

                new CardClass()
                {
                    Picture = "hearthstoneicon.jpg",
                    Name = "Neutral",
                    ClassColor = Color.DarkGoldenrod
                },
            };
            ListOfCardClasses.ItemsSource = CardClassList;
        }

        async private void ListOfClasses_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            var CardClassList = (ListView)sender;
            var GetCardClass = (CardClass)CardClassList.SelectedItem;
            await Navigation.PushAsync(new DeckBuilderPage(GetCardClass));
        }
    }
}